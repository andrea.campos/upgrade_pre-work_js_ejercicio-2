function ruedasDeJuguete(diametroRueda) {
    if (diametroRueda <= 10) {
        console.log("Es una rueda para un juguete pequeño");
    }
    if (diametroRueda > 10 && diametroRueda < 20) {
        console.log("Es una rueda para un juguete mediano");
    }
    if (diametroRueda >= 20) {
        console.log("Es una rueda para un juguete grande");
    }
}

var resultado = ruedasDeJuguete(5);